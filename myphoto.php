<?php
include 'inc/functions.php';
// will print only errors, but no warnings
error_reporting(E_ERROR);

//var_dump($_FILES);

// define variables and set to empty
$nameErr = $whichdirErr = $fileErr = "";
$name = $whichdir = $params = "";

/* test file selected, is image type, and is a JPG/JPEG/PNG format - $_FILES */
if (!exif_imagetype($_FILES["file"]["tmp_name"]))
    $fileErr = "No file selected or not an image file!";    
elseif (!check_if_jpg_png($_FILES["file"]))
    $fileErr = "Not a JPG/JPEG/PNG image!";    
elseif (!check_image_size($_FILES["file"]))
    $fileErr = "Image has to be less than 1MB!";
elseif (isset($_FILES["file"]) && is_uploaded_file($_FILES['file']['tmp_name'])) {
    
    $file_name  = $_FILES["file"]["name"];
    $file_temp  = $_FILES["file"]["tmp_name"];
    $file_size  = $_FILES["file"]["size"];
    $file_type  = $_FILES["file"]["type"];
    $file_error = $_FILES["file"]["error"];
    
    if ($file_error > 0) {
        /* message somewher else */
        $fileErr = "Something went wrong during file upload!";    
    }    
}

/* this part deals with name and directory values - $_POST */
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty($_POST["name"])) {
        $nameErr = "Your or photo name required";
    }
    else {
        $name = test_input($_POST["name"]);        
        // check if name only contains letters, whitespace and underscore
        if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
            $nameErr = "Only letters and white space allowed";
        }
        elseif (strlen($name) < 5) {
            $nameErr = "Minimum five characters";
        }
    }

    if (empty($_POST["whichdir"])) {
        $whichdirErr = "Choose destination directory";
    }
    else {
        $whichdir = $_POST["whichdir"];
    }

}

if (!empty($nameErr) || !empty($whichdirErr) || !empty($fileErr)) {
    $params .= "&name=" . urlencode($name);
    $params .= "&whichdir=" . urlencode($whichdir);        

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&whichdirErr=" . urlencode($whichdirErr);
    $params .= "&fileErr=" . urlencode($fileErr);

    header("Location: index.php?" . $params);
    exit;
}    

?>

<!DOCTYPE HTML>  
<html>
    
<head>
    <title>Register page</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">  
    <link rel="icon" href="images/favicon.png">
</head>
    
<body>      
    <div class="wrapper">
        <header class="header">
            <img  class="header__logo-image" src="images/phplogo.png" alt="Logo image" title="Logo image">            
        </header>        
        <!-- End of header -->
        <div class="main">
            
            <?php
                 if (empty($nameErr) || empty($whichdirErr) || empty($fileErr))
                    {

                        //var_dump(exif_read_data($file_temp));

                        $ext_temp  = explode(".", $file_name);
                        $extension = end($ext_temp);

                        $directory = "files/" . $whichdir . "/";
                        //echo $directory;

                        $newfilename = space_to_underscore($name) . "-" . time() . "-" . (string)rand(1, 9) . "." . $extension;
                        $upload = $directory . $newfilename; 

                        // check if directory already exists
                        if (!is_dir($directory)) {
                            mkdir($directory, 0755, true);
                        }

                        echo '<div class="output">';
                        echo "<h2>Your Input:</h2>";
                        //echo $upload;

                         if (!file_exists($upload)) {            
                            if (move_uploaded_file($file_temp, $upload)) {
                                echo "<p><b>Success!</b></p>";
                            } else {
                                echo "<p><b>There was an error!</b></p>";
                            }
                        }

                        //echo $upload;                                        
                        echo '<span class="listing">Your name: </span><br>' . $name;
                        echo "<br>";

                        echo '<span class="listing">Image location: </span><br>';
                        $currentworkingdir = getcwd();
                        //echo getcwd();
                        chdir($directory);
                        echo getcwd();
                        chdir($currentworkingdir);

                        echo "<br>";

                        echo '<span class="listing">Old file name: </span><br>' . $file_name;
                        echo "<br>";

                        echo '<span class="listing">New file name: </span><br>' . $newfilename;
                        echo "<br>";                    

                        echo '<span class="listing">Image preview: </span>';
                        echo '<div class="image-preview">';
                        echo "<img class=\"image\" src=\"$directory$newfilename\">";
                        echo '</div>';

                        echo "<br>";                    
                        echo "<a href=\"index.php\">Return to form</a>";        
                        echo '</div>';
                }                                   
            ?>   
            <div class="bottom-form">
                <form action="inc/empty.php" method="post">
                    <input type="submit" name="submitbutton" id="submitbutton" value="empty">
                </form>
            </div>
        </div>
    </div> 
</body>
    
</html>
