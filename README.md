# Project no. 8


## Task

This is my assignment for week no. 8. The main goal of the project is to allow user to upload a .jpeg image to folder of his choice (private or public). The basic form allows name, folder and image input. The page myphoto.php checks if input entries are valid. If not, a specific error messeage prompts user how to fix the problem. The myphoto.php page is suplied with 'empty' button which removes or created directories and uploaded files (if any).

## Update (4/1/2018)

All functions are stored in functions.php. Added two more functions. Now it's possible to upload PNG image as well. Image size should't be more than 1MB. The file 'myphoto.php' is moved to root.