<?php
// check if image extension is jpg or png
function check_if_jpg_png($file) {
    if (exif_imagetype($file["tmp_name"]) == 2 || exif_imagetype($file["tmp_name"]) == 3)
        return true;    
    else
        return false;
}

/*****************************************************/

// check if image is bigger than 1MB
function check_image_size($file) {
    if ($file["size"] < 1024 * 1024)
        return true;
    else
        return false;
}

/*****************************************************/

// function for deleting directory and its content
// taken from:
// https://paulund.co.uk/php-delete-directory-and-files-in-directory
function delete_directory($dirname) {
    $dir_handle = "";
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
        return false;
    while($file = readdir($dir_handle)) {
       if ($file != "." && $file != "..") {
           if (!is_dir($dirname."/".$file))
               unlink($dirname."/".$file);
           else
               delete_directory($dirname.'/'.$file);
           }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}

/*****************************************************/

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/*****************************************************/

function space_to_underscore($data) {
    return str_replace(' ', '_', $data);
}

/*****************************************************/
?>