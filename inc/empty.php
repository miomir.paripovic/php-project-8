<?php
include 'functions.php';

// define dir path to erase
$path1 = '../files/private';
$path2 = '../files/public';

delete_directory($path1);
delete_directory($path2);

header("Location: ../index.php");
exit;
?>
