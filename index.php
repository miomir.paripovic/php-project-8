<?php
    $name = $whichdir = "";
    $nameErr = $whichdirErr = $fileErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['whichdir'])) { $whichdir = $_GET['whichdir']; }
    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['whichdirErr'])) { $whichdirErr = $_GET['whichdirErr']; }
    if (isset($_GET['fileErr'])) { $fileErr = $_GET['fileErr']; }    
?>

<!DOCTYPE HTML>  

<html>

<head>
    <title>Project no. 8</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"> 
    <link rel="icon" href="images/favicon.png">
</head>

<body>  
    <div class="wrapper">
        <header class="header">
            <img  class="header__logo-image" src="images/phplogo.png" alt="Logo image" title="Logo image">            
        </header>        
        <!-- End of header -->
        <div class="main">             
             <form action="myphoto.php" method="post" name="upload" enctype="multipart/form-data">
                <fieldset class="fieldset">
                    <legend class="legend">Basic Info</legend>
                    <label for="name">Name<span class="required">*</span><span class="error"><?php echo $nameErr; ?></span></label>
                    <input type="text" name="name" class="std-input" placeholder="Enter your name or the file name" value="<?php echo $name; ?>">
                    <div class="radio-input">
                        <label>Choose directory:<span class="required">*</span><span class="error"><?php echo $whichdirErr; ?></span></label><br>
                        <label><input class="radio" type="radio" name="whichdir" value="private" <?php echo ($whichdir == 'private' ? 'checked="checked"' : '');?>>private</label><br>
                        <label><input class="radio" class="radio-end" type="radio" name="whichdir" value="public" <?php echo ($whichdir == 'public' ? 'checked="checked"' : ''); ?>>public</label><br>           
                    </div>
                    
                     <label for="inputfile">File:<span class="required">*</span><span class="error"><?php echo $fileErr; ?></span></label>
                     <input type="file" name="file" id="inputfile" /><br /><br />

                     <input type="submit" name="submitbutton" id="submitbutton" value="submit" />
                     <input type="reset" name="resetbutton" id="resetbutton" value="cancel" />
                </fieldset>                                                           
            </form>  
        </div>
    </div> 

</body>

</html>